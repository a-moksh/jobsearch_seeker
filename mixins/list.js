import _ from 'lodash'

import base from '~/mixins/base'

export default function(resource) {
  return _.merge({}, base(resource), {})
}
