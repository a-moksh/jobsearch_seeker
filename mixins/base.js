import _ from 'lodash'
import { Validator } from 'vee-validate'
import ja from 'vee-validate/dist/locale/ja'
import en from 'vee-validate/dist/locale/en'

export default function(resource) {
  return {
    head() {
      const title = this.pageHead('title')
      this.$store.commit('SET_PAGE_TITLE', title)
      if (this.$route.path === this.$i18n.path('/')) {
        return {
          title
        }
      }
      return {
        title: title
          ? `${title} - ${this.$t('global.title')}`
          : this.$t('global.title')
      }
    },
    fetch({ store }) {
      // return store.dispatch('langAsync', 'navigation')
    },
    methods: {
      scrollToClass(className) {
        let keyTrack = null
        _.each(this.$el.getElementsByClassName(className), (value, key) => {
          if (value.innerText !== '') {
            keyTrack = key
            return false
          }
        })
        if (keyTrack !== null) {
          const el = this.$el.getElementsByClassName(className)[keyTrack]
          el.scrollIntoView({
            behavior: 'smooth',
            block: 'nearest',
            inline: 'start'
          })
        }
      },
      fullToHalf(value) {
        let ascii = ''
        for (let i = 0, l = value.length; i < l; i++) {
          let c = value[i].charCodeAt(0)
          // make sure we only convert half-full width char
          if (c >= 0xff00 && c <= 0xffef) {
            c = 0xff & (c + 0x20)
          }
          ascii += String.fromCharCode(c)
        }
        return ascii
      },
      generateUrl(params) {
        const searchArr = []
        _.each(params, (value, key) => {
          if (value != null && value !== '00') {
            if (key === 'search') {
              value = encodeURI(value)
            }
            searchArr.push(`${key}=${value}`)
          }
        })
        return _.join(searchArr, '&')
      },
      showAdvanceSearch() {
        this.$store.commit(
          'SET_ADVANCE_SEARCH',
          !this.$store.getters.advanced_search
        )
      },

      showHideNoti() {
        this.$store.commit(
          'SET_NOTIFICATION_DRAWER',
          !(this.$store.getters.isAuthenticated
            ? this.$store.getters.notification_drawer
            : true)
        )
      },
      pageHead(key, props) {
        const page = this.page
        if (!_.isEmpty(page)) {
          return this.$t(`page.${page}.head.${key}`, props)
        }
        const rootKey = _.get(this.$options, 'config.headRootKey')
        if (!_.isEmpty(rootKey)) {
          const _key = `${rootKey}.${key}`
          return this.$t(_key, props)
        }
        const pageRootKey = _.get(this.$options, 'pageRootKey')
        if (!_.isEmpty(pageRootKey)) {
          return this.$t(`page.${pageRootKey}.head.${key}`, props)
        }
      },

      pageContent(key, props) {
        const page = this.page
        if (!_.isEmpty(page)) {
          return this.$t(`page.${page}.content.${key}`, props)
        }
        const rootKey = _.get(
          this.$options,
          'config.contentRootKey',
          this.$store.getters.pageRootKey
        )
        if (!_.isEmpty(rootKey)) {
          const _key = `${rootKey}.${key}`
          return this.$t(_key, props)
        }
        const pageRootKey = _.get(this.$options, 'pageRootKey')
        if (!_.isEmpty(pageRootKey)) {
          return this.$t(`page.${pageRootKey}.content.${key}`, props)
        }
      },

      /**
       * display attribute
       * @param {string} group
       * @param {string} value
       * @returns Array {*}
       */
      attribute(group, value) {
        return this.$store.getters.attribute({ group, value })
      },

      /**
       * display attribute
       * @param {string} group
       * @param {string} text
       * @returns Array {*}
       */
      attributeByText(group, text) {
        const items = this.$store.getters.attribute({ group })
        return _.find(items, { text })
      },

      veevalidateLocalize() {
        if (this.$store.getters.locale === 'en') {
          Validator.localize('en', en)
        } else {
          Validator.localize('ja', ja)
        }
      }
    }
  }
}
