export default function(resource) {
  return {
    methods: {
      async addRemoveWishlist(data) {
        const isAuthenticated = this.$store.getters.isAuthenticated
        if (!isAuthenticated) {
          this.$router.push(this.$i18n.path('/login'))
        } else {
          await this.$store.dispatch('jobs/addRemoveWishlists', data)
          const status = true
          if (data.page === 'user.wishlist') {
            window.location.reload(true)
          } else {
            return status
          }
        }
      },

      async CheckIsApplied(data) {
        const applied = await this.$store.dispatch('jobs/isApplied', {
          id: data.id
        })
        return applied
      }
    }
  }
}
