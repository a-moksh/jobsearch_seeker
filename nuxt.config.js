import dotenv from 'dotenv'
import pkg from './package'

dotenv.config()

export default {
    debug: process.env.DEBUG || false,
    env: process.env,
    mode: 'universal',

    /*
     ** Headers of the page
     */
    head: {
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,500,700|Material+Icons&amp;subset=japanese'
            }
        ]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#27B6C5',
        height: '4px'
    },
    // loading: false,
    router: {
        base: '/',
        middleware: ['auth', 'i18n', 'prelaunch'],
        scrollBehavior: function(to, from, savedPosition) {
            return { x: 0, y: 0 }
        }
    },
    /*
     ** Global CSS
     */
    css: ['~/assets/style/app.scss'],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/lodash.js',
        '~/plugins/axios.js',
        '~/plugins/i18n.js',
        '~/plugins/v-img.js',
        '~/plugins/vee-validate.js',
        '~/plugins/location-fetcher.js',
        '~/plugins/currency-formatter.js',
        '~/plugins/moment.js',
        '~/plugins/ua-parser.js',
        '~/plugins/common.js',
        { src: '~plugins/date-dropdown.js', ssr: false },
        { src: '~plugins/google-maps.js', ssr: false },
        { src: '~plugins/echo.js', ssr: false },
        { src: '~plugins/hls.js', ssr: false },
        { src: '~plugins/galary_slideshow.js', ssr: false }
    ],
    generate: {
        routes: ['/en', 'en/about', '/ja', '/ja/about']
    },

    /*
     ** Nuxt.js modules
     */
    modules: [
        '@nuxtjs/toast',
        '@nuxtjs/auth',
        '@nuxtjs/axios',
        '@nuxtjs/dotenv',
        'nuxt-fontawesome',
        'nuxt-healthcheck', ['bootstrap-vue/nuxt', { css: false }]
    ],

    fontawesome: {
        imports: [{
                set: '@fortawesome/free-solid-svg-icons',
                icons: ['fas']
            },
            {
                set: '@fortawesome/free-brands-svg-icons',
                icons: ['fab']
            }
        ]
    },

    bootstrapVue: {
        bootstrapCSS: false,
        bootstrapVueCSS: false
    },

    /*
     ** Nuxt.js healthcheck
     */
    healthcheck: {
        path: '/healthcheck.html',
        contentType: 'application/json',
        healthy: () => {
            return JSON.stringify({ result: 'pong' })
        }
    },

    toast: {
        position: 'bottom-right',
        duration: 4000,
        fullWidth: false
    },

    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: '/seeker/login',
                        method: 'post',
                        propertyName: 'access_token'
                    },
                    logout: { url: '/logout', method: 'post' },
                    user: { url: '/seeker/user', method: 'get', propertyName: 'data' }
                }
            }
        },
        redirect: false,
        localStorage: false
    },

    /*
     ** Axios module configuration
     */
    axios: {
        baseURL: process.env.API_URL
    },

    /*
     ** Build configuration
     */
    build: {
        // extractCSS: true,
        // optimization: {
        //   splitChunks: {
        //     minSize: 10000,
        //     maxSize: 250000
        //   }
        // },
        // transpile: [/^vue2-google-maps($|\/)/],
        // extend(config, ctx) {
        //   if (ctx.isDev && ctx.isClient) {
        //     config.devtool = '#source-map'
        //     config.module.rules.push({
        //       enforce: 'pre',
        //       test: /\.(js|vue)$/,
        //       loader: 'eslint-loader',
        //       exclude: /(node_modules)/
        //     })
        //   }
        // }
    }
}