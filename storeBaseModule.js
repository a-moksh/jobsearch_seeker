import _ from 'lodash'

export default function({ resource }) {
  return {
    state() {
      return {
        list: { data: [], meta: { total: 0 } },
        headers: [],
        columns: {},
        show: {},
        otherUpdate: {},
        otherUpdateFlash: false,
        currentId: -1,
        currentIndex: -1,
        loading: true,
        pagination: {
          descending: true
        },
        selected: [],
        echo: []
      }
    },
    actions: {
      /**
       * list all the item
       * @param  commit
       * @param { boolean } caching
       * @param { object } params
       */
      listAsync({ commit, dispatch }, { caching = true, params }) {
        return this.$axios.get(resource, { params }).then(response => {
          if (caching) {
            commit('SET_LIST', response.data)
            // dispatch('channel', { name: 'order' })
          }
          return response
        })
      }
    },
    mutations: {
      /**
       * set list
       * @param { object } state
       * @param { object } payload
       * @constructor
       */
      SET_LIST(state, payload) {
        state.list = payload
      }
    },
    getters: {
      /**
       * receive show
       * @param { object } state
       * @return {function(*=)}
       */
      show: state => id => {
        const _id = _.toInteger(id)
        const dataFromList = _.find(_.get(state, 'list.data', []), ['id', _id])
        const data = dataFromList ? { data: dataFromList } : state.show
        return _.toInteger(_.get(data, 'data.id', -1)) === _id
          ? data
          : undefined
      },
      /**
       * receive list
       * @param { object } state
       * @return {Object|*}
       */
      list(state) {
        return state.list
      }
    }
  }
}
