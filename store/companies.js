/* import Vue from 'vue'
import _ from 'lodash'
import store from '~/storeBaseModule'

const resource = 'users'

const base = store({ resource })

export default _.merge(base,{



  actions: {

    verifyEmail ({ commit }, VerifyData) {
      return this.$axios.post('user/verify/email',VerifyData);
    },

    register({ commit }, RegisterData) {
      return this.$axios.post('user/register',RegisterData)
    },

    forgotPassword({ commit }, forgotData){
      return this.$axios.post('user/password/forgot',forgotData)
    },

    resetPassword({ commit }, resetData){
      return this.$axios.post('user/password/reset',resetData)
    }

  }
}) */

export const state = () => ({
  company: {},
  companies: [],
  meta: {}
})

export const mutations = {
  SET_COMPANIES(state, payload) {
    state.companies = payload
  },

  SET_META(state, payload) {
    state.meta = payload
  },

  SET_COMPANY(state, payload) {
    state.company = payload
  }
}

export const getters = {
  companies: state => {
    return state.companies
  },

  meta: state => {
    return state.meta
  },

  company: state => {
    return state.company
  }
}

export const actions = {
  getCompaniesList({ commit }, searchData) {
    return this.$axios
      .get(`/providers?page=${searchData.page}&limit=6`)
      .then(response => {
        commit('SET_COMPANIES', response.data.data)
        commit('SET_META', response.data.meta)
      })
  },

  getCompany({ commit }, slug) {
    return this.$axios.get(`providers/${slug}`).then(response => {
      commit('SET_COMPANY', response.data.data)
    })
  }
}
