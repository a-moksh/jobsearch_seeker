import _ from 'lodash'
export const state = () => ({
  job: {},
  jobs: [],
  meta: {},
  applications: [],
  wishlists: [],
  wishlist_selected: []
})

export const mutations = {
  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_JOBS(state, payload) {
    state.jobs = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_WISHLIST_SELECTED(state, payload) {
    state.wishlist_selected = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_APPLICATIONS(state, payload) {
    state.applications = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_SAVED_WISHLISTS(state, payload) {
    state.wishlists = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_META(state, payload) {
    state.meta = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_WISHLIST(state, payload) {
    const job = _.findIndex(state.jobs, { id: payload.id })
    const wishlist = _.findIndex(state.wishlists, { id: payload.id })
    if (job !== -1) {
      state.jobs[job].isWishlisted = !payload.status
    }
    if (wishlist !== -1) {
      state.wishlists[wishlist].isWishlisted = !payload.status
    }
    if (!_.isEmpty(state.job)) state.job.isWishlisted = !payload.status
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_JOB(state, payload) {
    //  const job = _.findIndex(state.jobs, { id: payload.id });
    state.job = payload
  }
}

export const getters = {
  /**
   *
   * @param state
   * @returns {*}
   */
  jobs: state => {
    return state.jobs
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  wishlist_selected: state => {
    return state.wishlist_selected
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  meta: state => {
    return state.meta
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  job: state => {
    return state.job
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  applications: state => {
    return state.applications
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  wishlists: state => {
    return state.wishlists
  }
}

export const actions = {
  /**
   *
   * @param commit
   * @param searchData
   * @returns {Promise<AxiosResponse<any>>}
   */
  getJobsList({ commit }, searchData) {
    let searchQuery = ''
    _.each(searchData, (value, key) => {
      if (key !== 'page') {
        searchQuery += `&${key}=${value}`
      }
    })
    return this.$axios
      .get(
        `/jobs?page=${searchData.page || 1}&limit=${searchData.limit ||
          10}${searchQuery}`
      )
      .then(response => {
        commit('SET_JOBS', response.data.data)
        commit('SET_META', response.data.meta)
      })
  },

  /**
   *
   * @param commit
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  addRemoveWishlists({ commit }, data) {
    commit('SET_WISHLIST', data)
    return this.$axios
      .post('seeker/wishlists/job', { job_id: data.id })
      .then(response => {
        if (response.data.data.length > 0) {
          commit('SET_USER_SAVED_WISHLISTS', response.data.data)
          commit('SET_META', response.data.meta)
        }
      })
      .catch(() => {})
  },

  /**
   *
   * @param commit
   * @param data
   * @returns {AxiosPromise<any>}
   */
  beroreApplyJob({ commit }, data) {
    return this.$axios.post('seeker/job/apply/pre-check', data)
  },

  /**
   *
   * @param commit
   * @param data
   * @returns {AxiosPromise<any>}
   */
  applyJob({ commit }, data) {
    return this.$axios.post('seeker/job/apply', data)
  },

  /**
   *
   * @param commit
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  isApplied({ commit }, data) {
    return this.$axios
      .get(`seeker/job/apply/check/${data.id}`)
      .then(response => {
        return response.data
      })
      .catch(err => {
        return err.response
      })
  },

  /**
   *
   * @param commit
   * @param slug
   * @returns {Promise<AxiosResponse<any>>}
   */
  getJob({ commit }, slug) {
    return this.$axios.get(`jobs/${slug}`).then(response => {
      commit('SET_JOB', response.data.data)
    })
  },

  /**
   *
   * @param commit
   * @param searchData
   * @returns {Promise<AxiosResponse<any>>}
   */
  getUserApplication({ commit }, searchData) {
    return this.$axios
      .get(`seeker/job/applications?page=${searchData.page}&limit=10`)
      .then(response => {
        commit('SET_APPLICATIONS', response.data.data)
        commit('SET_META', response.data.meta)
      })
  },

  /**
   *
   * @param commit
   * @param searchData
   * @returns {Promise<AxiosResponse<any>>}
   */
  getUserWishlist({ commit }, searchData) {
    return this.$axios
      .get(
        `seeker/wishlists?page=${searchData.page}&limit=10&orderby=created_at`
      )
      .then(response => {
        commit('SET_USER_SAVED_WISHLISTS', response.data.data)
        commit('SET_META', response.data.meta)
      })
  }
}
