import _ from 'lodash'
export const state = () => ({
  locales: {
    ja: 'japanese',
    en: 'english'
  },
  locale: 'ja',
  messages: {},
  auth: null,
  attributes: {},
  snackBar: { display: false },
  dialog: { display: false },
  navigations: [],
  breadCrumbs: [],
  pageTitle: '',
  errorMessages: [],
  notification_drawer: false,
  advanced_search: false,
  notifications: []
})

export const mutations = {
  ADD_NOTIFICATIONS(state, payload) {
    debugger
    const notiData = {
      created_at: '',
      read_at: null,
      data: {}
    }
    const pushData = {
      message_en: payload.message_en,
      message_ja: payload.message_ja,
      redirect_seeker: payload.redirect_seeker
    }
    notiData.data = pushData
    state.notifications.data.unshift(notiData)
  },
  SET_NOTIFICATIONS(state, payload) {
    state.notifications = payload
  },
  SET_IS_LOGGEDIN(state, payload) {
    state.auth.loggedIn = payload
  },
  SET_AUTH_USER(state, payload) {
    state.auth.user = payload
  },

  SET_USER_IMAGE(state, payload) {
    const image = {
      media_value: payload.data,
      id: payload.id
    }
    state.auth.user.image = image
  },
  /**
   * user auth
   * @param { object } state
   * @param { object } payload
   * @constructor
   */
  SET_AUTH(state, payload) {
    state.auth = payload
    if (process.browser) {
      if (_.isEmpty(payload)) {
        localStorage.removeItem('auth')
        sessionStorage.removeItem('auth')
        return
      }
      if (_.get(payload, 'remember_me', false)) {
        localStorage.setItem('auth', JSON.stringify(payload))
      } else {
        sessionStorage.setItem('auth', JSON.stringify(payload))
      }
    }
  },

  /**
   * set attribute
   * @param { object } state
   * @param { object } payload
   * @constructor
   */
  SET_ATTRIBUTES(state, payload) {
    state.attributes = _.merge({}, _.cloneDeep(state.attributes), payload)
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_LOCALE(state, payload) {
    if (_.hasIn(state, `locales.${payload}`)) {
      state.locale = payload
      this.app.i18n.locale = payload
    }
  },
  SET_PAGE_TITLE(state, payload) {
    state.pageTitle = payload
  },
  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_MESSAGES(state, payload) {
    state.messages = _.merge({}, _.cloneDeep(state.messages), payload)
  },

  SET_ERROR_MESSAGES(state, payload) {
    state.errorMessages = payload.error
  },
  SET_NOTIFICATION_DRAWER(state, payload) {
    state.notification_drawer = payload
  },
  SET_ADVANCE_SEARCH(state, payload) {
    state.advanced_search = payload
  }
}

export const getters = {
  notifications: state => {
    return state.notifications
  },

  advanced_search: state => {
    return state.advanced_search
  },

  notification_drawer: state => {
    return state.notification_drawer
  },
  /**
   * all locales
   * @param { object } state
   */
  locales: state => {
    return state.locales
  },
  /**
   * client locale
   * @param { object } state
   * @return {string|*}
   */
  locale: state => {
    return state.locale
  },
  /**
   *
   * @param state
   * @return {*}
   */
  messages(state) {
    return state.messages
  },
  pageTitle(state) {
    return state.pageTitle
  },

  /**
   * get attributes
   * @param { object } state
   * @return {function({group: *, value?: *})}
   */
  attribute: state => ({ group, value }) => {
    const _value = _.toString(value)
    const items = _.orderBy(
      _.get(state, `attributes.${group}`, []),
      'order',
      'asc'
    )
    if (_.size(_value) > 0) {
      return _.find(items, ['value', _value]) || { text: null, value: _value }
    }
    return items
  },

  isAuthenticated: state => {
    return state.auth.loggedIn
  },

  loggedInUser: state => {
    return state.auth.user
  },

  errorMessages: state => {
    return state.errorMessages
  },

  toast: state => {
    return state.toast
  }
}

export const actions = {
  /**
   *
   * @param commit
   * @param id
   * @returns {Promise<AxiosResponse<any>>}
   */
  readNotification({ commit }, id) {
    return this.$axios
      .post('user/notifications/' + id.id)
      .then(response => {
        return true
      })
      .catch(() => {
        return false
      })
  },

  /**
   *
   * @param commit
   * @returns {Promise<AxiosResponse<any>>}
   */
  getNotifications({ commit }) {
    return this.$axios.get('user/notifications').then(response => {
      commit('SET_NOTIFICATIONS', response.data)
    })
  },

  /**
   *
   * @param commit
   * @param dispatch
   * @param key
   * @returns {AxiosPromise<any>}
   */
  imageAsync({ commit, dispatch }, key) {
    return this.$axios.get(`seeker/images/${key}`, {
      responseType: 'blob',
      headers: {
        ContentType: 'image/jpeg'
      }
    })
  },

  /**
   *
   * @param state
   * @param commit
   * @return {PromiseLike<T> | Promise<T>}
   * @private
   */
  _langAsync({ state, commit }, key) {
    let messages = {}
    if (!_.isEmpty(key)) {
      const locale = state.locale
      messages = _.get(state.messages, `${locale}.${key}`, {})
      if (_.isEmpty(messages)) {
        const resource = 'langs/' + locale
        return this.$axios
          .get(resource, {
            params: { key }
          })
          .then(response => {
            messages = response.data.data
            commit('SET_MESSAGES', _.set({}, `${locale}.${key}`, messages))
            this.app.i18n.mergeLocaleMessage(
              locale,
              _.get(state.messages, locale)
            )
            return messages
          })
      }
    }
    return Promise.resolve(messages)
  },
  /**
   * language
   * @param dispatch
   * @param { string } key
   * @return {*}
   */
  langAsync({ dispatch }, key) {
    if (_.isArray(key)) {
      const keys = _.cloneDeep(key)
      return Promise.all(_.map(keys, key => dispatch('_langAsync', key)))
    }
    return dispatch('_langAsync', key)
  },

  /**
   * attribute information
   * @param { object } state
   * @param commit
   * @param { object} group
   * @return {Promise<{}>}
   */
  attributeAsync({ state, commit }, group) {
    const data = {}
    const groups = _.castArray(group)
    const cachedGroups = []
    _.forEach(groups, groupName => {
      if (_.hasIn(state, `attributes.${groupName}`)) {
        cachedGroups.push(groupName)
        _.set(data, groupName, _.get(state, `attributes.${groupName}`))
      }
    })
    _.pullAll(groups, cachedGroups)
    if (_.isEmpty(groups)) {
      return Promise.resolve(data)
    }
    const params = { limit: 1000 }
    _.set(params, 'group', groups.join(','))
    return this.$axios.get('attributes', { params }).then(response => {
      if (response.status === 200) {
        _.forEach(response.data.data, item => {
          if (!_.hasIn(data, item.group.name)) {
            _.set(data, item.group.name, [])
          }
          _.get(data, item.group.name).push({
            text: item.name,
            value: item.value,
            order: item.display_order,
            group_name: item.group.name,
            self: item.self,
            parent: item.parent_value
          })
        })
        commit('SET_ATTRIBUTES', data)
      }
    })
  },

  loactionZipcode({ state, commit }, zipcode) {
    return this.$axios
      .get('zipcode/' + zipcode.zip_code)
      .then(response => {
        response.status = 'OK'
        return response
      })
      .catch(() => {
        const err = {}
        err.status = 'NOTOK'
        return err
      })
  }
}
