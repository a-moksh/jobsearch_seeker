export const state = () => ({
  profile: {},
  message: {}
})

export const mutations = {
  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_PROFILE(state, payload) {
    state.profile = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_MESSAGE(state, payload) {
    state.message = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER(state, payload) {},

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_EXPERIENCES(state, payload) {
    state.profile.seeker_experiences = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_EDUCATIONS(state, payload) {
    state.profile.seeker_educations = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_CERTIFICATIONS(state, payload) {
    state.profile.seeker_certifications = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_SETTINGS(state, payload) {
    state.profile.seeker_settings = payload
  },

  /**
   *
   * @param state
   * @param payload
   * @constructor
   */
  SET_USER_SKILLS(state, payload) {
    state.profile.seeker_skills = payload
  }
}

export const getters = {
  /**
   *
   * @param state
   * @returns {*}
   */
  message: state => {
    return state.message
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  profile: state => {
    return state.profile.seeker_detail
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  loggedInUserDetail: state => {
    return state.profile.user_detail
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  certificate: state => {
    return state.profile.seeker_certifications
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  experiences: state => {
    return state.profile.seeker_experiences
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  education: state => {
    return state.profile.seeker_educations
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  setting: state => {
    return state.profile.seeker_settings
  },

  /**
   *
   * @param state
   * @returns {*}
   */
  skills: state => {
    return state.profile.seeker_skills
  }
}

export const actions = {
  /**
   *
   * @param commit
   * @param messageId
   * @returns {AxiosPromise<any>}
   */
  updateMessageRead({ commit }, messageId) {
    return this.$axios.put(`seeker/scout-mails/read/${messageId}`)
  },
  /**
   *
   * @param commit
   * @param mediaObj
   * @returns {AxiosPromise<any>}
   */
  getSignedUrl({ commit }, mediaObj) {
    return this.$axios.get(
      `seeker/media/pre-signed-url?type=${mediaObj.type}&extension=${mediaObj.extension}`
    )
  },

  /**
   *
   * @param commit
   * @param messageId
   * @returns {Promise<AxiosResponse<any>>}
   */
  getMessages({ commit }, messageId) {
    return this.$axios.get(`seeker/scout-mails/${messageId.id}`).then(res => {
      commit('SET_MESSAGE', res.data.data)
      return res.data.data
    })
  },

  /**
   *
   * @param commit
   * @param replyData
   * @returns {AxiosPromise<any>}
   */
  replyScoutAppication({ commit }, replyData) {
    return this.$axios.put(`seeker/scout-mails/${replyData.id}`, replyData.data)
  },

  /**
   *
   * @param commit
   * @param resendData
   */
  resendEmailVerification({ commit }, resendData) {
    // return this.$axios.post('user/verify/email/resend',resendData)
  },

  /**
   *
   * @param commit
   * @param VerifyData
   * @returns {AxiosPromise<any>}
   */
  verifyEmail({ commit }, VerifyData) {
    return this.$axios.post('seeker/verify/email', VerifyData)
  },

  /**
   *
   * @param commit
   * @param VerifyData
   * @returns {Promise<AxiosResponse<any>>}
   */
  preVerifyEmail({ commit }, VerifyData) {
    return this.$axios
      .post('verify/email/pre', { token: VerifyData.token })
      .then(res => {
        return res.data
      })
  },

  /**
   *
   * @param commit
   * @param RegisterData
   * @returns {AxiosPromise<any>}
   */
  register({ commit }, RegisterData) {
    return this.$axios.post('seeker/register', RegisterData)
  },

  /**
   *
   * @param commit
   * @param forgotData
   * @returns {AxiosPromise<any>}
   */
  forgotPassword({ commit }, forgotData) {
    return this.$axios.post('password/forgot', forgotData)
  },

  /**
   *
   * @param commit
   * @param resetData
   * @returns {AxiosPromise<any>}
   */
  resetPassword({ commit }, resetData) {
    return this.$axios.post('password/reset', resetData)
  },

  /**
   *
   * @param commit
   * @param id
   * @returns {Promise<AxiosResponse<any>>}
   */
  getSeekerProfileInfo({ commit }, id) {
    return this.$axios.get('seeker/seekers/' + id).then(response => {
      commit('SET_PROFILE', response.data.data)
    })
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateBasicProfile({ commit }, updateData) {
    return this.$axios.put(
      'seeker/seeker-user/' + updateData.id,
      updateData.data
    )
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateResumeProfile({ commit }, updateData) {
    return this.$axios.put('seeker/seekers/' + updateData.id, updateData.data)
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateExperiencesProfile({ commit }, updateData) {
    return this.$axios.put('seeker/seeker-experiences-bulk', {
      experiences: updateData.data
    })
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateEducationsProfile({ commit }, updateData) {
    return this.$axios.put('seeker/seeker-educations-bulk', {
      educations: updateData.data
    })
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateCertificationsProfile({ commit }, updateData) {
    return this.$axios.put('seeker/seeker-certificates-bulk', {
      certificates: updateData.data
    })
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateSkillsProfile({ commit }, updateData) {
    return this.$axios.put('seeker/seeker-skills-bulk', {
      skills: updateData.data
    })
  },

  /**
   *
   * @param commit
   * @param updateData
   * @returns {AxiosPromise<any>}
   */
  updateSeekerSetting({ commit }, updateData) {
    return this.$axios.put(
      'seeker/seeker-mail-settings/' + updateData.id,
      updateData.data
    )
  },

  /**
   *
   * @param commit
   * @param deleteData
   * @returns {AxiosPromise}
   */
  deleteProfileItem({ commit }, deleteData) {
    return this.$axios.delete(`seeker/${deleteData.resource}/${deleteData.id}`)
  },

  /**
   *
   * @param commit
   * @param deleteData
   * @returns {AxiosPromise}
   */
  contactUs({ commit }, contactData) {
    return this.$axios.post('contact-us', contactData)
  }
}
