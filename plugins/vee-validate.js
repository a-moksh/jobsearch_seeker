import Vue from 'vue'
import VeeValidate from 'vee-validate'
const dictionary = {
  en: {
    messages: {
      verify_password: 'The password must be alpha numeric and 8-15 length',
      validate_date: 'Please select the valid date',
      required_date: 'Please date field is required',
      accept_terms: 'You must accept terms and condition to register',
      katakana: 'Please enter only katakana characters',
      romanji: 'Please enter only english aplabhet characters',
      romanji_number_only: 'Please enter only romanji half-width numbers'
    }
  },
  ja: {
    messages: {
      verify_password: 'パスワードは英数字で8-15の長さでなければなりません',
      validate_date: '有効な日付を選択してください',
      required_date: '有効な日付を選択してください',
      accept_terms: '登録するには利用規約に同意する必要があります',
      katakana: 'カタカナで入力してください',
      romanji: '半角英字で入力してください',
      romanji_number_only: '半角数字のみを入力してください'
    }
  }
}
VeeValidate.Validator.localize(dictionary)
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeField'
})

VeeValidate.Validator.extend('romanji_number_only', {
  validate: value => {
    const strongRegex = new RegExp('^[0-9]+$')
    return strongRegex.test(value)
  }
})

VeeValidate.Validator.extend('verify_password', {
  validate: value => {
    const strongRegex = new RegExp(
      '^(?=.*\\d)(?=.*[a-z])[\\w~@#$%^&*+=`|{}:;!.?\\"()\\[\\]-]+$'
    )
    return strongRegex.test(value) && value.length >= 8 && value.length <= 15
  }
})

VeeValidate.Validator.extend('accept_terms', {
  validate: value => {
    return parseInt(value)
  }
})

VeeValidate.Validator.extend('validate_date', {
  validate: value => {
    const dateProvided = value.split('.')
    if (dateProvided.includes('NaN')) return false
    const dateConverted = Date.parse(
      `${dateProvided[2]}/${dateProvided[1]}/${dateProvided[0]}`
    )
    return !isNaN(dateConverted)
  }
})
VeeValidate.Validator.extend('required_date', {
  validate: value => {
    return value != null && value !== '0000-00-00' && value !== ''
  }
})

VeeValidate.Validator.extend('katakana', {
  validate: value => {
    const katakanaReg = new RegExp('[ァ-ン 　]')
    const katakanaFullWidth = new RegExp('[ァーン 　]')
    const valueArr = value.split('')
    let test = true
    for (let i = 0; i < valueArr.length; i++) {
      if (test === false) return false
      test =
        (katakanaReg.test(valueArr[i]) ||
          katakanaFullWidth.test(valueArr[i])) &&
        !/^[a-zA-Z]+$/.test(valueArr[i])
    }
    return test
  }
})

VeeValidate.Validator.extend('romanji', {
  validate: value => {
    const romanjiReg = new RegExp('^[a-zA-Z]+$')
    const valueArr = value.split('')
    let test = true
    for (let i = 0; i < valueArr.length; i++) {
      if (test === false) return false
      test = romanjiReg.test(valueArr[i])
      if (valueArr[i] === ' ') test = true
    }
    return test
  }
})
