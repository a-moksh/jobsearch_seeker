class FetchLocation {
  constructor(context) {
    this.context = context
  }

  fetchFromDb(request) {
    return this.context.$axios
      .get(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${request.zipCode}&key=AIzaSyDFAXyLWEAvRGqVb1ZNBjp4bjN-wC5HFIc&language=${request.lang}`
      )
      .then(response => {})
  }

  fetch(request) {
    return this.context.$axios
      .get(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${request.zipCode}&key=AIzaSyDFAXyLWEAvRGqVb1ZNBjp4bjN-wC5HFIc&language=${request.lang}`
      )
      .then(response => {
        const addressFetched = {}
        if (response.data.status === 'OK') {
          addressFetched.status = response.data.status
          const components = response.data.results[0].address_components
          if (components.length === 5) {
            addressFetched.pref = components[3].long_name
            addressFetched.city = components[2].long_name
            addressFetched.addr = components[1].long_name
          } else if (components.length === 6) {
            addressFetched.pref = components[4].long_name
            addressFetched.city =
              components[3].long_name + components[2].long_name
            addressFetched.addr = components[1].long_name
          } else if (components.length === 4) {
            addressFetched.city = components[2].long_name
            addressFetched.addr = components[1].long_name
          }
        } else if (response.data.status === 'ZERO_RESULTS') {
          addressFetched.status = response.data.status
        }
        return addressFetched
      })
      .catch(err => {
        return err
      })
  }

  geocode(request) {
    return this.context.$axios
      .get(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${request.address}&key=AIzaSyCpSVXvu6iOE0f-hs6o7e_RJxQ44F3qA1I&language=${request.lang}`
      )
      .then(response => {
        const addressFetched = {}
        if (response.data.status === 'OK') {
          addressFetched.status = response.data.status
          addressFetched.lat = response.data.results[0].geometry.location.lat
          addressFetched.lng = response.data.results[0].geometry.location.lng
        } else if (response.data.status === 'ZERO_RESULTS') {
          addressFetched.status = response.data.status
        }
        return addressFetched
      })
      .catch(err => {
        return err
      })
  }

  currency(number) {
    const currSym = ['百', '千', '万', '円', '億']
    let finalResult = ''
    let reminder = number
    let run = true
    while (run) {
      if (reminder >= 100000000) {
        finalResult += Math.floor(reminder / 100000000) + currSym[4]
        reminder = number % 100000000
      } else if (reminder >= 10000) {
        finalResult += Math.floor(reminder / 10000) + currSym[2]
        reminder = number % 10000
      } else if (reminder >= 1000) {
        finalResult += Math.floor(reminder / 1000) + currSym[1]
        reminder = reminder % 1000
      } else {
        finalResult += reminder + currSym[3]
        run = false
      }
    }
    return finalResult + ' 円'
  }
}

export default (context, inject) => {
  inject('FetchLocation', new FetchLocation(context))
}
