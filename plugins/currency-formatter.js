class CurrencyFormatter {
  constructor(context) {
    this.context = context
  }

  currency(number) {
    const currSym = ['百', '千', '万', '円', '億']
    let finalResult = ''
    let reminder = number
    let run = true
    while (run) {
      if (reminder >= 100000000) {
        finalResult += Math.floor(reminder / 100000000) + currSym[4]
        reminder = number % 100000000
      } else if (reminder >= 10000) {
        finalResult += Math.floor(reminder / 10000) + currSym[2]
        reminder = number % 10000
      } else if (reminder >= 1000) {
        finalResult += Math.floor(reminder / 1000) + currSym[1]
        reminder = reminder % 1000
      } else {
        if (reminder !== 0) finalResult += reminder + currSym[3]
        run = false
      }
    }
    return finalResult
  }
}

export default (context, inject) => {
  inject('CurrencyFormatter', new CurrencyFormatter(context))
}
