import Hls from 'hls.js'

export default ({ app, env }, inject) => {
  inject('hls', Hls)
}
