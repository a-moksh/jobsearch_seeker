import Vue from 'vue'

import videojs from 'video.js'
Vue.use(videojs, {
  controls: true,
  autoplay: false,
  fluid: false,
  loop: false,
  width: 320,
  height: 240,
  controlBar: {
    volumePanel: false
  },
  plugins: {
    // configure videojs-record plugin
    record: {
      audio: false,
      video: true,
      debug: true
    }
  }
})
