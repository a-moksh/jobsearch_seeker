import UAParser from 'ua-parser-js'

export default ({ app, env }, inject) => {
  inject('UAParser', UAParser())
}
