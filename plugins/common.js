import _ from 'lodash'
// Define a common function
export default ({ app, store, route, env }, inject) => {
  // Scroll to the given selector
  inject('scrollTo', selector => {
    if (process.client) {
      _.delay(() => {
        const el =
          document !== undefined ? document.querySelector(selector) : null
        if (el) {
          window.scroll(0, el.offsetTop)
        }
      }, 500)
    }
  })
}
