import Vue from 'vue'
import DateDropdown from 'vue-date-dropdown'

Vue.component('date-dropdown', DateDropdown)
