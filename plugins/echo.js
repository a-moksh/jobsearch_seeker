import Echo from 'laravel-echo'
import io from 'socket.io-client'
import _ from 'lodash'
export default ({ app, env }, inject) => {
  window.io = io
  const echo = new Echo({
    broadcaster: 'socket.io',
    host: env.WS_CONNECT,
    auth: {
      headers: {
        Authorization: _.has(app, '$auth.getToken')
          ? app.$auth.getToken('local')
          : null
      }
    }
  })
  inject('echo', echo)
}
