export default function({ store, $axios, redirect, app, nuxt }) {
  $axios.interceptors.request.use(config => {
    if (
      !config.url.includes('maps.googleapis.com') &&
      !config.url.includes('amazonaws.com')
    ) {
      config.headers['Accept-Language'] = store.state.locale
      // config.headers['ContentType'] = 'application/json'
      // config.headers['Accept'] = 'application/json'
      config.headers.Authorization = app.$auth.getToken('local') || ''
    } else {
      delete config.headers.common.Authorization
    }

    return config
  })

  $axios.onError(e => {
    const code = parseInt(e.response && e.response.status)
    if (code === 403) {
      app.$auth.logout()
      // redirect(`/${store.getters.locale}/login`);
    }
  })
}
