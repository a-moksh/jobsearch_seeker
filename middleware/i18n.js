import _ from 'lodash'
import languageParser from 'accept-language-parser'

export default function({
  req,
  isHMR,
  app,
  store,
  route,
  params,
  error,
  redirect
}) {
  if (process.server) {
    const acceptLocale =
      languageParser.pick(
        _.keys(store.state.locales),
        _.get(req.headers, 'accept-language')
      ) || app.i18n.fallbackLocale
    app.i18n.fallbackLocale = acceptLocale
  }

  const defaultLocale = app.i18n.fallbackLocale
  // If middleware is called from hot module replacement, ignore it
  if (isHMR) return
  // Get locale from params
  const locale = params.lang || defaultLocale

  if (!_.hasIn(store, `state.locales.${locale}`)) {
    return error({ message: 'This page could not be found.', statusCode: 404 })
  }
  if (route.fullPath.indexOf(`/${locale}`) < 0) {
    return redirect(`/${locale}${route.fullPath}`)
  }
  store.commit('SET_LOCALE', locale)
}
